
from django.db import models
from django.db.models.deletion import CASCADE, SET_NULL

from multiselectfield import MultiSelectField


class BioSystematics(models.Model):
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=CASCADE)
    name = models.CharField('Наименование', max_length=255)
    name_lat = models.CharField(
        'Наименование lat', max_length=255, blank=True, null=True)
    description = models.TextField('Описание', blank=True, null=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Биологический вид'
        verbose_name_plural = 'Биологические виды'

    def __str__(self):
        return '{} ({})'.format(self.name, self.name_lat)


class Habitus(models.Model):
    parent = models.ForeignKey('self', blank=True, null=True, on_delete=CASCADE)
    name = models.CharField('Наименование', max_length=255)
    name_lat = models.CharField(
        'Наименование lat', max_length=255, blank=True, null=True)
    description = models.TextField('Описание', blank=True, null=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Габитус'
        verbose_name_plural = 'Габитус'

    def __str__(self):
        return '{} ({})'.format(self.name, self.name_lat)


class HardinessZone(models.Model):
    zone = models.CharField('Зона', max_length=4)
    temperature_min = models.IntegerField('Темп. мин.', blank=True, default=0)
    temperature_max = models.IntegerField('Темп. макс.', blank=True, default=0)

    class Meta:
        ordering = ('-zone',)
        verbose_name = 'Зона морозостойкости'
        verbose_name_plural = 'Зоны морозостойкости'

    def __str__(self):
        return self.zone


class PhytoMorphology(models.Model):
    name = models.CharField('Наименование', max_length=255)
    name_lat = models.CharField(
        'Наименование lat', max_length=255, blank=True, null=True)
    description = models.TextField('Описание', blank=True, null=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Фито-морфология'
        verbose_name_plural = 'Фито-морфология'

    def __str__(self):
        return self.name


class GrowingLocation(models.Model):
    name = models.CharField('Наименование', max_length=255)
    name_lat = models.CharField(
        'Наименование lat', max_length=255, blank=True, null=True)
    description = models.TextField('Описание', blank=True, null=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Где растет'
        verbose_name_plural = 'Где растет'

    def __str__(self):
        return self.name


class Plant(models.Model):

    LIGHT_BR_SUN = 1
    LIGHT_LV_SUN = 2
    LIGHT_SIDE_SHADOW = 3
    LIGHT_FULL_SHADOW = 4
    LIGHTS = (
        (LIGHT_BR_SUN, 'Яркое солнце'),
        (LIGHT_LV_SUN, 'Любит свет'),
        (LIGHT_SIDE_SHADOW, 'Боковое затенение'),
        (LIGHT_FULL_SHADOW, 'Полная тень'),
    )

    plant_type = models.PositiveSmallIntegerField(
        'Тип', choices=((1, 'Хвойные'), (2, 'Лиственные'),))

    name = models.CharField('Наименование', max_length=255)
    name_lat = models.CharField(
        'Наименование lat', max_length=255, blank=True, null=True)

    habitus = models.ForeignKey(Habitus, verbose_name='Габитус', max_length=255,
                                blank=True, null=True, on_delete=SET_NULL)
    kind = models.ForeignKey(BioSystematics, verbose_name='Вид', max_length=255,
                             blank=True, null=True, on_delete=SET_NULL)
    kind_count = models.PositiveIntegerField('Кол-во видов', default=1,
                                             blank=True)
    where_is_growing = models.ManyToManyField(
        GrowingLocation, verbose_name='Где растет', blank=True)
    altitudo = models.IntegerField('Высота', blank=True, null=True)
    hardiness_zone = models.ManyToManyField(HardinessZone, blank=True)
    age = models.PositiveIntegerField('Возраст', blank=True, null=True)
    age_in_kind = models.PositiveIntegerField('Возраст в культуре', blank=True,
                                              null=True)
    light = MultiSelectField('Свет/тень', choices=LIGHTS, max_length=255,
                             blank=True, null=True)
    wind_resistance = models.PositiveSmallIntegerField(
        'Ветроустойчивость', blank=True, null=True, choices=(
            (0, 'Неустойчивое'),
            (1, 'Слабое'),
            (2, 'Удовлетворительное'),
            (3, 'Хорошее'),
            (4, 'Сильное'),
        ))
    drought_tolerance = models.PositiveSmallIntegerField(
        'Засухоустойчивость', blank=True, null=True, choices=(
            (0, 'Неустойчивое'),
            (1, 'Слабое'),
            (2, 'Удовлетворительное'),
            (3, 'Хорошее'),
            (4, 'Сильное'),
        ))
    city_tolerance = models.PositiveSmallIntegerField(
        'Засухоустойчивость', blank=True, null=True, choices=(
            (0, 'Неустойчивое'),
            (1, 'Слабое'),
            (2, 'Удовлетворительное'),
            (3, 'Хорошее'),
            (4, 'Сильное'),
        ))
    fat_land = models.NullBooleanField('Плодородная почва', blank=True)
    ph = models.PositiveSmallIntegerField('PH', blank=True, null=True)
    height = models.PositiveSmallIntegerField('Высота', blank=True, null=True)
    diameter = models.PositiveSmallIntegerField('Диаметер', blank=True,
                                                null=True)

    class Meta:
        ordering = ('name',)
        verbose_name = 'Хвойные'
        verbose_name_plural = 'Хвойные'

    def __str__(self):
        return self.name
