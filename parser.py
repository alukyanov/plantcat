# -*- encoding: utf-8 -*-
import json


def main():
    catalog = {
        "class": {},  # класс
        "family": {},  # семейство
        "genus": {},  # род
        "subgenus": {},  # подрод
    }

    conifers = []
    deciduous = []

    with open('hvoinye.csv', 'rb') as hvoinye_csv:

        klass = None
        family = None
        rod = None
        podrod = None

        for i, l in enumerate(hvoinye_csv.readlines()[1:]):
            items = l.split('|')
            itm0 = items[0]

            ssalc = {v: k for k, v in catalog['class'].items()}
            ylimaf = {v: k for k, v in catalog['family'].items()}
            suneg = {v: k for k, v in catalog['genus'].items()}
            sunegbus = {v: k for k, v in catalog['subgenus'].items()}

            if itm0.decode('utf-8').strip().lower().startswith(u'класс'):
                if itm0 in ssalc:
                    klass = ssalc[itm0]
                else:
                    klass = len(ssalc) + 1
                    catalog['class'][klass] = itm0
                family = None
                rod = None
                podrod = None
                continue

            elif itm0.decode('utf-8').strip().lower().startswith(u'семейство'):
                if itm0 in ylimaf:
                    family = ylimaf[itm0]
                else:
                    family = len(ylimaf) + 1
                    catalog['family'][family] = itm0
                rod = None
                podrod = None
                continue

            elif itm0.decode('utf-8').strip().lower().startswith(u'род'):
                if itm0 in suneg:
                    rod = suneg[itm0]
                else:
                    rod = len(suneg) + 1
                    catalog['genus'][rod] = itm0
                podrod = None
                continue

            elif itm0.decode('utf-8').strip().lower().startswith(u'подрод'):
                if itm0 in sunegbus:
                    podrod = sunegbus[itm0]
                else:
                    podrod = len(sunegbus) + 1
                    catalog['subgenus'][podrod] = itm0
                continue

            print i, klass, family, rod, podrod
            try:
                vid = items[1]
                gde_rastet = items[2]
                morozostoikost = items[3]
                vysota = items[4]
                gabitus = items[5]
                temp_rosta = items[6]
                vozrast = items[7]
                svet_ten = items[8]
                zasuhoustoichivost = items[9]
                vetrostoikost = items[10]
                ustoichivost_v_gorode = items[11]
                plodorodnost_pochva = items[12]
            except IndexError as e:
                print i, l
                raise e

            conifers.append(dict(
                id=i,
                k_vidov=itm0,
                klass=klass,
                family=family,
                rod=rod,
                podrod=podrod,
                vid=vid,
                gde_rastet=gde_rastet,
                vysota=vysota,
                gabitus=gabitus,
                temp_rosta=temp_rosta,
                vozrast=vozrast,
                svet_ten=svet_ten,
                zasuhoustoichivost=zasuhoustoichivost,
                vetrostoikost=vetrostoikost,
                morozostoikost=morozostoikost,
                ustoichivost_v_gorode=ustoichivost_v_gorode,
                plodorodnost_pochva=plodorodnost_pochva,
            ))
    catalog['conifers'] = conifers
    catalog['deciduous'] = deciduous

    json.dump(catalog, open('hvoinye.json', 'w'))


if __name__ == '__main__':
    main()
